import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor() {
    const config = {
      apiKey: "AIzaSyAu9xN1M39SLSCE0VTzaVHZEM-okKK-a_o",
      authDomain: "ocr-angular-book.firebaseapp.com",
      databaseURL: "https://ocr-angular-book.firebaseio.com",
      projectId: "ocr-angular-book",
      storageBucket: "ocr-angular-book.appspot.com",
      messagingSenderId: "353556584395"
  	};
  	firebase.initializeApp(config);
  }
}

